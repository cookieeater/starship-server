// Setting variables
var express = require('express');
var app = express();
var server = app.listen(process.env.PORT || 3000, listen);
var socket_io = require('socket.io');
var io = socket_io(server);

var players = {};

io.sockets.on('connection',setup_socket);

function listen() {
  var host = server.address().address;
  var port = server.address().port;
  console.log('Example app listening at http://' + host + ':' + port);
  players = {};
}

function setup_socket(socket){

  console.log("We got a new enemy");

  socket.on('ship', function(data) {
    players[socket.id] = data;
    socket.broadcast.emit('enemies', players);
  });  

  socket.on('shot', function(data){
    data.owner = socket.id;
    socket.broadcast.emit('shot', data);
  });

  socket.on('disconnect', function(){
    console.log("Player "+socket.id+" disconnected");
    delete players[socket.id];
    socket.broadcast.emit('disconnected', socket.id);
  });

  socket.on('newEnemy', function(data){
    console.log("New enemy has entered: "+socket.id);
    data.id = socket.id;
    players[socket.id] = data;
    socket.broadcast.emit('enemies', players);
  });

  socket.on('score', function(score){
    console.log("Ship "+socket.id+ " scores");
    var data = {
      'id':socket.id
    }
  })

  
}